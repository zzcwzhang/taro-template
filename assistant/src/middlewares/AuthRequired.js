import { View } from '@tarojs/components';
import { useRouter } from '@taroShared/utils/route';
import { useEffect } from 'react';

import { useRecoilValue } from 'recoil';
import { authInfo$ } from '@taroShared/stores/auth';

const loginPath = '/pages/login/index';

export default function (props) {
  const { children = null } = props;

  const router = useRouter();

  const authInfo = useRecoilValue(authInfo$);
  const userId = authInfo?.user?.id;

  useEffect(() => {
    if (!userId) {
      router.replace(loginPath, true);
    }
  }, []);

  return !userId ? (
    <View className="loading p-4">
      <View className="p-3">登录中...</View>
    </View>
  ) : (
    children
  );
}
