import _ from 'lodash';
import { selector, atom, atomFamily, selectorFamily } from 'recoil';
import cache, { CACHE_USER, CACHE_TEAM } from '@/utils/cache';

const BASE_FIX = 'stores/auth';

export const _authInfo$ = atom({
  key: `${BASE_FIX}_authInfo$`,
  default: {
    team: cache.get(CACHE_TEAM),
    user: cache.get(CACHE_USER)
  }
});
