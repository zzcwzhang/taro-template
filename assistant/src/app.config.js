export default defineAppConfig({
  pages: ['pages/home/index', 'pages/assistant/index', 'pages/list/index'],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: '致远数智助手',
    navigationBarTextStyle: 'black'
  },
  usingComponents: {
    iconfont: `@/components/iconfont/${process.env.TARO_ENV}/${process.env.TARO_ENV}`
  },
  subPackages: [
    {
      root: 'pages/login',
      pages: ['index']
    }
  ],
  tabBar: {
    color: '#666666',
    selectedColor: '#067BF7',
    list: [
      {
        iconPath: 'assets/image/home_unselected.png',
        selectedIconPath: 'assets/image/home_selected.png',
        pagePath: 'pages/home/index',
        text: '首页'
      },
      {
        iconPath: 'assets/image/data_unselected.png',
        selectedIconPath: 'assets/image/data_selected.png',
        pagePath: 'pages/list/index',
        text: '数据'
      },
      {
        iconPath: 'assets/image/data_unselected.png',
        selectedIconPath: 'assets/image/data_selected.png',
        pagePath: 'pages/assistant/index',
        text: '数据助手'
      }
    ]
  }
});
