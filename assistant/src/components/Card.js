import IconFont from '@/components/iconfont';

export default function Card(props) {
  const { title, iconName = null, children } = props;

  return (
    <div className="bg-white rounded-lg">
      <div
        className="h-12 border-b border-b-slate-100 grid justify-start content-center pl-4 gap-x-2 grid-flow-col"
        style={{ borderBottomStyle: 'solid' }}
      >
        {iconName && <IconFont name={iconName} size="40" />}
        {title}
      </div>
      <div className="min-h-28 p-4">{children}</div>
    </div>
  );
}
