/* eslint-disable */
import React, { FunctionComponent } from 'react';

interface Props {
  name: 'a-Tab1icon' | 'zhuzhuangtu1' | 'loudoutu1' | 'a-Tab2icon' | 'mianjitu1' | 'jibenxinxi1' | 'huanxingtu1' | 'huanxingtu' | 'mianjitu' | 'zhuzhuangtu' | 'loudoutu' | 'jibenxinxi';
  size?: number;
  color?: string | string[];
  style?: React.CSSProperties;
}

declare const IconFont: FunctionComponent<Props>;

export default IconFont;
