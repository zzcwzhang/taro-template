import { View } from '@tarojs/components';

export default function List() {
  return (
    <View className="index">
      <View className="w-96 shadow rounded container">
        <View className="text-[#acc855] p-3 bg-blue-100">Hello List</View>
      </View>
    </View>
  );
}
