export default function () {
  return (
    <div className="index">
      <div className="w-96 shadow rounded container">
        <div className="text-red-600 p-3 bg-blue-100">Hello Assistant</div>
      </div>
    </div>
  );
}
