export default definePageConfig({
  navigationBarTitleText: '首页',
  usingComponents: {
    iconfont: `@/components/iconfont/${process.env.TARO_ENV}/${process.env.TARO_ENV}`
  }
});
