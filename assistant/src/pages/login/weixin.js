import Taro from '@tarojs/taro';
import axios from 'axios';
import cache, {
  CACHE_ACCESS_TOKEN,
  CACHE_USER,
  CACHE_TEAM,
  CACHE_API_PATH
} from '@taroShared/utils/cache';
import { possibleBaseUrlList } from '@taroShared/utils/ajax';

console.log('possibleBaseUrlList:', possibleBaseUrlList);

// 通过code获取unionId,调用哪个API都可以
const getWeixinUserInfo = async (code) => {
  const url = `${possibleBaseUrlList[0]}/api/usercenter/thirdPlatform/miniWeixin.getDataAssistantUserInfo`;
  const result = await axios({
    method: 'POST',
    url,
    data: {
      code
    }
  });

  return result?.data?.data;
};

const trialLogin = async (unionId) => {
  const result = await Promise.any(
    possibleBaseUrlList.map(async (apiPath) => {
      const res = await axios({
        method: 'POST',
        url: `${apiPath}/api/usercenter/thirdPlatform/miniWeixin.loginDataAssistant`,
        data: {
          unionId
        }
      });
      const { success, data } = res?.data;
      if (success) {
        return { data, apiPath };
      } else {
        throw new Error(`${success} ${data}`);
      }
    })
  );
  return result;
};

export const getCode = async () => {
  const { code, errMsg } = await Taro.login();
  if (errMsg && errMsg !== 'login:ok') {
    throw errMsg;
  }
  return code;
};

export default async () => {
  const code = await getCode();

  const { unionid: unionId = '' } = await getWeixinUserInfo(code);

  if (!unionId) {
    throw new Error('get auth info must have either code or unionId');
  }

  // 尝试性登录
  const result = await trialLogin(unionId);

  const { data, apiPath } = result;

  if (data) {
    const { user, token, team } = data;
    cache.set(CACHE_API_PATH, apiPath);
    cache.set(CACHE_USER, user);
    cache.set(CACHE_TEAM, team);
    cache.set(CACHE_ACCESS_TOKEN, token);
    return { user, team };
  }
};
