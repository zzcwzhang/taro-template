import { useEffect } from 'react';
import { View } from '@tarojs/components';
import { useRouter } from '@taroShared/utils/route';

import { clear as cacheClear } from '@taroShared/utils/cache';
import ajax from '@taroShared/utils/ajax';
import { isWeixin, isDebug } from '@taroShared/utils/system';

import { useSetRecoilState } from 'recoil';
import { authInfo$ } from '@taroShared/stores/auth';

import weixinLogin from './weixin';

export default function () {
  const setAuthInfo = useSetRecoilState(authInfo$);

  const router = useRouter();

  useEffect(() => {
    const init = async () => {
      try {
        let _authInfo;
        if (isDebug) {
          _authInfo = await ajax('/api/usercenter/auth.info');
        } else if (isWeixin) {
          _authInfo = await weixinLogin();
        }
        if (_authInfo) {
          setAuthInfo(_authInfo);

          router.replaceToRecord();
        } else {
          throw new Error('not found auth');
        }
      } catch (err) {
        cacheClear();
        setAuthInfo(undefined);
        throw err;
      }
    };
    init();
  }, []);

  return (
    <View className="loading p-4">
      <View className="p-3">登录中...</View>
    </View>
  );
}
