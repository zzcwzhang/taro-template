import { RecoilRoot } from 'recoil';
import { useDebug } from '@taroShared/utils/debug';
import '@taroShared/utils/lodash-pre';
import './app.less';

function App({ children }) {
  useDebug();
  return <RecoilRoot>{children}</RecoilRoot>;
}

export default App;
