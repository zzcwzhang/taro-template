function createWhenTs (params) {
  return params.typescript ? true : false
}

const handler = {
  '/global.d.ts': createWhenTs,
  '/tsconfig.json': createWhenTs,
  '/src/pages/index/index.js' ({ pageName }) {
    return { setPageName: `/src/pages/${pageName}/${pageName}.js` }
  },
  '/src/pages/index/index.css' ({ pageName}) {
    return { setPageName: `/src/pages/${pageName}/${pageName}.css` }
  }
}

const basePageFiles = [
  '/src/pages/index/index.js',
  '/src/pages/index/index.css'
]

module.exports = {
  handler,
  basePageFiles
}
