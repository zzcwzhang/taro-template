const baseConfig = require('prettier-config-custom');

const config = {
  ...baseConfig
  // 自定义
  // ...
};

module.exports = config;
